<?php
namespace API;

use Luracast\Restler\RestException;

class User extends Model implements \Luracast\Restler\iAuthenticate {

	use UserAuthentication, Traits\Relations;

	protected static $customAttributes = [
		'methods' => ['get_name' => 'name'],
	];

	/**
	 * Finds all users, optionally filtered.
	 * Gets all users and return their data, optionally filtering by group. Returns all fields and custom attributes.
	 *
	 * @param int $group Returns only users that belong to this group, if specified
	 * @throws RestException 404 ID not found
	 * @return array all attributes from all {@link \Model\User} entries
	 */
	public function index($group = null) {
		if ($group)
			return self::_findMeBy('Group', $group);
		else
			return parent::index();
	}

	/**
	 * Creates a new user on the server.
	 * Inserts the new user in the database and returns the new object, including the "id" field.
	 *
	 * @status 201 Created
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 405 ID supplied; if you want to edit, use PUT instead
	 *
	 * @param string $username {@from body} User login
	 * @param string $name {@from body} User complete name
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\User} attributes
	 */
	protected function post($username = null, $name = null, $request_data = null) {
		self::_verifyRequiredFields(compact('username', 'name'));
		return $this->_processPost(compact('username', 'name'), $request_data);
	}

	/**
	 * Updates a user on the server.
	 * Updates data from a page and returns the updated object.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @param int $id The user's ID
	 * @param string $username {@from body} User login
	 * @param string $name {@from body} User complete name
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\User} attributes
	 */
	protected function put($id, $username = null, $name = null, array $request_data = null) {
		return $this->_processPut($id, compact('username', 'name'), $request_data);
	}
}

trait UserAuthentication {

	/**
	 * Authenticates the user.
	 * Receives the username and password and authenticates in the server,
	 * giving back a session cookie.
	 *
	 * @param string $user {@from body} username
	 * @param string $password {@from body} Plain-text password
	 *
	 * @return array { key: { name: "key_name", value: "key_value" } }
	 * @throws RestException 401 Invalid login information
	 */
	public function postLogin($user, $password) {
		$exists = \Model\User::where('username', $user)->exists();
		if ($exists && $this->_LDAPAuthentication($user, $password)) {
			session_set_cookie_params(2 * YEAR, '/', null, HTTPS_ENABLED);
			session_name(SESSION_NAME);
			session_start();
			return ['token' => ['name' => SESSION_NAME, 'value' => session_id()]];
		}
		else {
			throw new RestException(401, 'Invalid login information.');
		}
	}

    /**
     * Logs out the currently logged in user
     * Removes the session cookie that was previously set by logging in
     *
     * @return string
     *
     */
    public function postLogout() {
        setcookie(SESSION_NAME, null, -1000);
        session_destroy();
        return "";
    }

	public function __isAllowed() {
		return true;
	}

	/**
	 * Authenticates the user in the LDAP Server.
	 * @todo still to be implemented
	 *
	 * @param string $user Username
	 * @param string $password User password, in plain text
	 * @return bool
	 */
	protected function _LDAPAuthentication($user, $password) {
		return true;
	}

}