<?php
namespace API;

use Helper\Arrays;
use Luracast\Restler\RestException;

/**
 * Abstract class that should be used by all API classes that interact directly with a specific model.
 *
 * The name of the classes that inherit from this class should be the same as the model's class name (but
 * of course, in the API namespace).
 * Some method were already basic implemented here, such as {@link index}, {@link get} and {@link delete},
 * as they are generic enough and only need the ID (or not even it). More specific methods such as {@link post}
 * and {@link put}, that require specific arguments and maybe some treatment on them, should be implemented calling
 * {@link _processPost} and {@link _processPut}, that already implement the generic logic for those procedures.
 *
 * If there's any verb that should not be responded to in an specific resource, the method should be overwritten
 * and should simply call {@link _nothingHere}.
 *
 * If the model related to the class that inherits from this one has a different class name, then {@link _modelClass}
 * should be overriden. Example: \API\Field giving data about \Model\DefinitionField; \API\Field should have
 * <code>protected function _modelClass() { return '\Model\DefinitionField'; }</code>.
 *
 * @package API
 */
abstract class Model {

	/**
	 * List of additional serialization options. Will be used in internal methods to serialize the models into arrays
	 * with the correct fields.
	 * @see \ActiveRecord\Serialization
	 * @see \ActiveRecord\Model::to_array
	 * @var array
	 */
	protected static $customAttributes = [];

	/**
	 * List of all entries available in the server for the given user.
	 * Returns a list of all entries available in the server for the given user.
	 *
	 * @return array list of {@link \ActiveRecord\Model} entries' attributes
	 */
	protected function index() {
		$models = [];
		foreach (self::_callOnModel('all') as $model)
			$models[] = $model->attributesToArray();

		return $models;
	}

	/**
	 * Finds an entry and returns its fields.
	 * Returns the entry fields, including custom attributes.
	 *
	 * @param int $id Returns the specific entry information
	 * @throws RestException 404 ID not found
	 * @return array attributes from {@link \ActiveRecord\Model}
	 */
	protected function get($id) {
		return self::_find($id)->toArray();
	}

	/**
	 * Should receive the correct arguments and use {@link _processPost} to finish the procedure.
	 * Should also define proper documentation so the Swagger generator can work correctly.
	 * @return array attributes from a \Model objects
	 */
	abstract protected function post();

	/**
	 * Creates a new entry on the server.
	 * Inserts the new entry in the database and returns the entire new object, including the "id" and custom parameters.
	 *
	 * @param array $attributes the attributes to be changed. Null values are ignored.
	 * @param array $request_data the $request_data that came from the original Restler request.
	 *
	 * @throws \Luracast\Restler\RestException
	 *
	 * @return array the {@link \ActiveRecord\Model} attributes
	 */
	protected function _processPost(array $attributes, array $request_data) {
		if (isset($request_data['id']))
			throw new RestException(HTTP_METHOD_NOT_ALLOWED, 'ID supplied; if you want to edit, use PUT instead');

		return self::_callOnModel('create', [$attributes])->toArray(static::$customAttributes);
	}

	/**
	 * Should receive the correct arguments and use {@link _processPost} to finish the procedure.
	 * Should also define proper documentation so the Swagger generator can work correctly.
	 *
	 * @param int $id the entry that's going to be updated
	 *
	 * @return array attributes from the updated \Model object
	 */
	abstract protected function put($id);

	/**
	 * Updates an existing entry on the server.
	 * Updates the fields of an entry in the database and returns the updated object.
	 *
	 * @param int $id ID of the model to be updated
	 * @param array $attributes the attributes to be changed, in `name => value` format. Null values are ignored.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @return array the {@link \ActiveRecord\Model} attributes
	 */
	protected function _processPut($id, array $attributes) {
		$model = self::_find($id);
		$model->fill(Arrays::clear($attributes, false, 'is_null'));
		$model->update();
		return $model->toArray();
	}

	/**
	 * Removes an entry from the server.
	 * Deletes an entry from the server and returns a simple OK message.
	 *
	 * @throws RestException 404 Entry not found
	 *
	 * @param int $id The entry's ID
	 * @return array
	 * A response with a key "success" with a "message" telling the resource was deleted
	 */
	protected function delete($id) {
		self::_find($id)->delete();
		return Responder::simpleMessage('Resource deleted');
	}

	protected static function _nothingHere() {
		throw new RestException(404);
	}

	/**
	 * Helper method to pretify calls with the static model class.
	 * Can be overriden if the API class name differs from the model class name.
	 * @return string complete class name, with full-qualified namespaces
	 */
	static protected function _modelClass() {
		$model_class = get_called_class();
		return '\Model\\'.substr($model_class, strrpos($model_class, '\\')+1);
	}

	/**
	 * Helper method to pretify static method calls to the model
	 * @param $method the method to be called
	 * @param array $args The arguments to be sent to the method
	 * @return mixed
	 */
	static protected function _callOnModel($method, array $args = []) {
		return forward_static_call_array([static::_modelClass(), $method], $args);
	}

	/**
	 * Helper method to call {@link \ActiveRecord\Model::_find} on the current model.
	 * @param mixed $args,... The arguments to be sent to the method
	 * @return \ActiveRecord\Model|\ActiveRecord\Model[]
	 */
	static protected function _find() {
		return self::_callOnModel('find', func_get_args());
	}

	/**
	 * Verifies required fields when Restler is not able to do so.
	 *
	 * According to [this bug](https://github.com/Luracast/Restler/issues/142), Restler is unable to verify required
	 * fields when a method inherits from a generic one in an upper class (as we have on {@link put} and {@link post}.
	 * Thus, we have to verify by ourselves if there's any empty field.
	 *
	 * Example:
	 * <code>self::_verifyRequiredFields(compact('name', 'url'));</code>
	 *
	 * @param array $fields associative array with the key being the field name and the value, it's value.
	 *
	 * @throws \Luracast\Restler\RestException 400 Bad Request, just like Restler would do
	 */
	static protected function _verifyRequiredFields(array $fields) {
		foreach ($fields as $field => $value) {
			if (!$value)
				throw new RestException(HTTP_BAD_REQUEST, "`$field` is required but missing.");
		}
	}

}