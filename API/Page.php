<?php
namespace API;

use Luracast\Restler\RestException;

/**
 * API class for the system pages.
 * @package API
 *
 * @todo implement RBAC
 */
final class Page extends Model {

	use Traits\Relations;

	/**
	 * Finds all pages, optionally filtered.
	 * Gets all pages and return their data, optionally filtering by group access.
	 * Returns all fields and custom attributes.
	 *
	 * @param int $group Returns only pages accessible to this group, if specified
	 * @throws RestException 404 ID not found
	 * @return array
	 * all attributes from all {@link \Model\Page} entries
	 */
	public function index($group = null) {
		if ($group)
			return self::_findMeBy('Group', $group);
		else
			return parent::index();
	}

	/**
	 * Creates a new page on the server.
	 * Inserts the new page in the database and returns the entire new object, including the "id" and "hashed_url" parameters.
	 *
	 * @status 201 Created
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 405 ID supplied; if you want to edit, use PUT instead
	 *
	 * @param string $name {@from body} Page title
	 * @param string $url {@from body} Page URL
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Page} attributes
	 */
	protected function post($name = null, $url = null, $request_data = null) {
		self::_verifyRequiredFields(compact('name', 'url'));
		return $this->_processPost(compact('name', 'url'), $request_data);
	}

	/**
	 * Updates a page on the server.
	 * Updates data from a page and returns the updated object.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @param int $id The page's ID
	 * @param string $name {@from body} Page title
	 * @param string $url {@from body} Page URL
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Page} attributes
	 */
	protected function put($id, $name = null, $url = null, array $request_data = null) {
		return $this->_processPut($id, compact('name', 'url'), $request_data);
	}

	/**
	 * Lists all definition IDs from page.
	 * Lists the IDs only of all definitions that pertain to the specified page.
	 * If you need complete data, use <code> GET /definition?page={id} </code>
	 *
	 * @param int $id The Page ID
	 * @param boolean $complete If a complete definition should be given. Defaults to false, meaning only the bare
	 * definition data and its word are returned. If sent empty will work as "true"
	 *
	 * @return array List of IDs of definitions in the page
	 *
	 * @smart-auto-routing false
	 * @url GET {id}/definition
	 */
	protected function getDefinition($id, $complete = false) {
		$definitions = $this->_getRelation('PageHasDefinition', ['page_id' => $id], 'definition_id');
		
		if ($complete || (isset($_GET['complete']) && $_GET['complete'] == ''))
			$models = \Model\Definition::find_complete($definitions);
		else
			$models = \Model\Definition::find_partial($definitions);

		//in case we receive only one model, we should still return an array
		return (isset($models['id']))? [$models] : $models;
	}

	/**
	 * Adds a definition to a page.
	 * Includes the given definition into the specified page.
	 *
	 * @param int $id The Page ID
	 * @param int $definition_id The Definition ID
	 *
	 * @return array a simple success message
	 *
	 * @smart-auto-routing false
	 * @url POST {id}/definition/{definition_id}
	 */
	protected function postDefinition($id, $definition_id) {
		return $this->_addRelation('PageHasDefinition', ['page_id' => $id, 'definition_id' => $definition_id], 'Definition added to page');
	}

	/**
	 * Removes a definition from a page.
	 * Deletes the given definition from the specified page.
	 *
	 * @param int $id The Page ID
	 * @param int $definition_id The Definition ID
	 *
	 * @return array a simple success message
	 *
	 * @smart-auto-routing false
	 * @url DELETE {id}/definition/{definition_id}
	 */
	protected function deleteDefinition($id, $definition_id) {
		return $this->_deleteRelation('PageHasDefinition', ['page_id' => $id, 'definition_id' => $definition_id], 'Definition removed from page');
	}

}
