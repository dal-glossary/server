<?php
namespace API;

use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use LaravelBook\Ardent\InvalidModelException;
use Luracast\Restler\RestException;

class Restler extends \Luracast\Restler\Restler {

	protected function runApiMethod($o, $accessLevel) {
		try {
			return parent::runApiMethod($o, $accessLevel);
		}
		catch (ModelNotFoundException $e) {
			$this->handleError(HTTP_NOT_FOUND);
		}
		catch (MassAssignmentException $e) {
			Responder::$errorDetails = $e->getMessage();
			throw new RestException(HTTP_BAD_REQUEST, 'Field cannot be set');
		}
		catch (InvalidModelException $e) {
			Responder::$errorDetails = $e->getErrors();
			throw new RestException(HTTP_BAD_REQUEST, 'invalid data given');
		}
		catch (QueryException $e) {
            list($code, $error, $message) = $e->errorInfo;

            $catchDetail = function($regexp) use ($message) {
                preg_match($regexp, $message, $detail);
                return array_key_exists(1, $detail)? $detail[1] : '::UNKNOWN::';
            };

			switch($code) {
				case 23000: //Key Error
					switch ($error) {
                        case 1062: //duplicated key
                            $key = $catchDetail('/for key \'([\w\d_-]*)\'/');
                            Responder::$errorDetails = [$key => ['must be unique']];
                            throw new RestException(HTTP_BAD_REQUEST, 'duplicated values');
                            break;

                        case 1451: //would break relation
                            preg_match('/`([\w\d_-]*)`, CONSTRAINT/', $message, $table);
                            $table = array_key_exists(1, $table)? $table[1] : 'unknown table';
                            throw new RestException(HTTP_BAD_REQUEST, "There are still existent entries related to this record on table `$table`");
                            break;

                        case 1452: //non-existent FK value
                            preg_match('/FOREIGN KEY \(`([\w\d_-]*)`\)/', $message, $fk);
                            $fk = array_key_exists(1, $fk)? $fk[1] : 'unknown FK';
                            throw new RestException(HTTP_BAD_REQUEST, "invalid value specified for `$fk`. Expecting valid ID.");
                            break;

						default:
							throw new RestException(HTTP_BAD_REQUEST, "invalid value specified for some key. Error code [$error].");
							break;
					}
					break;

				default:
					throw new \RuntimeException($e->getMessage().' '.$e->getSqlBindings(), (int)$e->getCode(), $e);
					break;
			}
		}
	}

}