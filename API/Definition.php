<?php
namespace API;

use Helper\Arrays;
use Luracast\Restler\RestException;

/**
 * API class for the system definition roots.
 * @package API
 *
 * @todo implement RBAC
 */
final class Definition extends Model {

	use Traits\Relations;

	/**
	 * Finds all definitions.
	 * Gets all definitions and return their data, optionally filtering by related page.
	 * Returns all fields and custom attributes.
	 *
	 * @param int $page Returns all definitions from this page ID
	 * @throws RestException 404 ID not found
	 * @return array all attributes from all {@link \Model\Definition} entries
	 */
	public function index($page = null) {
		if ($page) {
			$mainModel = \Model\Page::with('definitions', 'definitions.word')->find($page);
			return \Model\Definition::find_partial($mainModel->definitions);
		}
		else {
			return \Model\Definition::find_partial('all');
		}
	}

	/**
	 * Returns a definition, optionally complete.
	 * Gives a defintion entry based on an ID and optionally returning the entire information, including the exact word,
	 * creator, and all fields organized by votes.
	 * Date elements have the following keys: "date", "timezone_type" and "timezone".
	 * @param int $id The definition ID
	 * @param bool $complete  If a complete definition should be given. Defaults to false, meaning only the bare
	 * definition data and its word are returned. If sent empty will work as "true"
	 * @throws RestException 404 Not Found
	 * @return array
	 */
	public function get($id, $complete = false) {
		if ($complete || (isset($_GET['complete']) && $_GET['complete'] == '')) {
			return \Model\Definition::find_complete($id);
		}
		else {
			return \Model\Definition::find_partial($id);
		}
	}

	/**
	 * Creates a new definition root on the server.
	 * Inserts the new definition root in the database and returns the new object.
	 *
	 * @status 201 Created
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 405 ID supplied; if you want to edit, use PUT instead
	 *
	 * @param int $word_id {@from body} The word's ID
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Page} attributes
	 */
	protected function post($word_id = null, $request_data = null) {
		self::_verifyRequiredFields(compact('word_id'));
		return $this->_processPost(compact('word_id'), $request_data);
	}

	/**
	 * Updates a definition root entry on the server.
	 * Updates data from a definition entry and returns the updated object.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @param int $id The definition's ID
	 * @param int $word_id {@from body} The word's ID
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Page} attributes
	 */
	protected function put($id, $word_id = null, array $request_data = null) {
		return $this->_processPut($id, compact('word_id'), $request_data);
	}

}