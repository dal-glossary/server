<?php
namespace API;

class Responder extends \Luracast\Restler\Responder {

	/**
	 * Details that should be sent along with the error message
	 * @var array
	 */
	public static $errorDetails = [];

	/**
	 * Whether the returned message should be encapsulated in a "success" key
	 * @var bool
	 */
	public static $encapsulateInSuccess = false;

	/**
	 * A simple wrapper to create some syntatic sugar for simple API methods, like this:
	 * <code>
	 * protected function delete($id) {
	 *     \Model\XXX::find($id)->delete();
	 *     return Responder::simpleMessage('Resource deleted');
	 * }
	 * </code>
	 *
	 * Using this method is equivalent to issuing <code>Responder::$encapsulateInSuccess = true</code> and then
	 * return a string.
	 *
	 * @param string $msg What will be returned to the user
	 * @return string the same $msg value
	 */
	public static function simpleMessage($msg) {
		self::$encapsulateInSuccess = true;
		return $msg;
	}

	public function formatResponse($response) {
		$result = parent::formatResponse($response);

		if (self::$encapsulateInSuccess)
			return ['success' => $result];
		else
			return $result;
	}

	public function formatError($code, $message) {
		$response = ['error' => compact('code', 'message')];

		if (self::$errorDetails)
			$response['error']['details'] = self::$errorDetails;

		return $response;
	}

}