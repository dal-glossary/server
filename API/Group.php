<?php
namespace API;

use Luracast\Restler\RestException;

/**
 * API class for the system words.
 * @package API
 *
 * @todo implement RBAC
 */
final class Group extends Model {

	use Traits\Relations;

	/**
	 * Creates a new group on the server.
	 * Inserts the new group in the database and returns the new object.
	 *
	 * @status 201 Created
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 405 ID supplied; if you want to edit, use PUT instead
	 *
	 * @param string $name {@from body} The group name
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Group} attributes
	 */
	protected function post($name = null, $request_data = null) {
		self::_verifyRequiredFields(compact('name'));
		return $this->_processPost(compact('name'), $request_data);
	}

	/**
	 * Updates a group on the server.
	 * Updates data from a group and returns the updated object.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @param int $id The group's ID
	 * @param string $name {@from body} The group name
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Group} attributes
	 */
	protected function put($id, $name = null, array $request_data = null) {
		return $this->_processPut($id, compact('name'), $request_data);
	}

	/**
	 * Lists all user IDs from group.
	 * Lists the IDs only of all users that pertain to the specified group.
	 * If you need complete data, use <code> GET /user?group={id} </code>
	 *
	 * @param int $id The Group ID
	 *
	 * @return array List of IDs of users in the group
	 *
	 * @smart-auto-routing false
	 * @url GET {id}/user
	 */
	protected function getUser($id) {
		return $this->_getRelation('GroupHasUser', ['group_id' => $id], 'user_id');
	}

	/**
	 * Adds a user to a group.
	 * Includes the given user into the specified group.
	 *
	 * @param int $id The Group ID
	 * @param int $user_id The User ID
	 *
	 * @return array
	 * a simple success message
	 *
	 * @smart-auto-routing false
	 * @url POST {id}/user/{user_id}
	 */
	protected function postUser($id, $user_id) {
		return $this->_addRelation('GroupHasUser', ['group_id' => $id, 'user_id' => $user_id], 'User added to group');
	}

	/**
	 * Removes a user from a group.
	 * Deletes the given user from the specified group.
	 *
	 * @param int $id The Group ID
	 * @param int $user_id The User ID
	 *
	 * @return array
	 * a simple success message
	 *
	 * @smart-auto-routing false
	 * @url DELETE {id}/user/{user_id}
	 */
	protected function deleteUser($id, $user_id) {
		return $this->_deleteRelation('GroupHasUser', ['group_id' => $id, 'user_id' => $user_id], 'User removed from group');
	}

	/**
	 * Lists IDs from all pages accessible to a group.
	 * Lists the IDs only of all pages that are viewable to the specified group.
	 * If you need complete data, use <code> GET /page?group={id} </code>
	 *
	 * @param int $id The Group ID
	 *
	 * @return array List of IDs of pages accessible to the group
	 *
	 * @smart-auto-routing false
	 * @url GET {id}/page
	 */
	protected function getPage($id) {
		return $this->_getRelation('GroupHasPage', ['group_id' => $id], 'page_id');
	}

	/**
	 * Adds a page to the access list of a group.
	 * Includes the given page into the specified group's access list.
	 *
	 * @param int $id The Group ID
	 * @param int $page_id The Page ID
	 *
	 * @return array
	 * a simple success message
	 *
	 * @smart-auto-routing false
	 * @url POST {id}/page/{page_id}
	 */
	protected function postPage($id, $page_id) {
		return $this->_addRelation('GroupHasPage', ['group_id' => $id, 'page_id' => $page_id], 'Page accessible to group');
	}

	/**
	 * Removes a page from the group's access list.
	 * Deletes the given page from the specified group's access list.
	 *
	 * @param int $id The Group ID
	 * @param int $page_id The Page ID
	 *
	 * @return array
	 * a simple success message
	 *
	 * @smart-auto-routing false
	 * @url DELETE {id}/page/{page_id}
	 */
	protected function deletePage($id, $page_id) {
		return $this->_deleteRelation('GroupHasPage', ['group_id' => $id, 'page_id' => $page_id], 'Page not accessible to group anymore');
	}

}