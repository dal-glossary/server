<?php
namespace API;

use Luracast\Restler\RestException;

/**
 * API class for the system words.
 * @package API
 *
 * @todo implement RBAC
 */
final class Word extends Model {

    use Traits\Relations;

    /**
     * Finds all words, the current user has access to, optionally filtered by page.
     *
     * @param int $page_id Returns only words that belong to the given page, if specified
     * @throws RestException 404 ID not found
     * @return array all attributes from all {@link \Model\Word} entries
     */
    public function index($page_id = null) {
        if ($page_id)
            return self::_findMeBy('Page', $page_id);
        else
            return parent::index();
    }

	/**
	 * Creates a new word on the server.
	 * Inserts the new word in the database and returns the new object.
	 *
	 * @status 201 Created
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 405 ID supplied; if you want to edit, use PUT instead
	 *
	 * @param string $name {@from body} The word
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Word} attributes
	 */
	protected function post($name = null, $request_data = null) {
		$w = new \Model\Word();
		self::_verifyRequiredFields(compact('name'));
		return $this->_processPost(compact('name'), $request_data);
	}

	/**
	 * Updates a word on the server.
	 * Updates data from a word and returns the updated object.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @param int $id The word's ID
	 * @param string $name {@from body} The word
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\Word} attributes
	 */
	protected function put($id, $name = null, array $request_data = null) {
		return $this->_processPut($id, compact('name'), $request_data);
	}

 }
