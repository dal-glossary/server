<?php
namespace API;

use Luracast\Restler\RestException;

/**
 * API class for the field types used in word definitions.
 * @package API
 *
 * @todo implement RBAC
 */
final class FieldType extends Model {

	/**
	 * Creates a new definition field on the server.
	 * Inserts the new field in the database and returns the new object.
	 *
	 * @status 201 Created
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 405 ID supplied; if you want to edit, use PUT instead
	 *
	 * @param string $name {@from body} The field name
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\FieldType} attributes
	 */
	protected function post($name = null, $request_data = null) {
		self::_verifyRequiredFields(compact('name'));
		return $this->_processPost(compact('name'), $request_data);
	}

	/**
	 * Updates a definition field on the server.
	 * Updates data from a field and returns the updated object.
	 *
	 * @throws RestException 400 Invalid data given
	 * @throws RestException 404 ID not found
	 *
	 * @param int $id The field's ID
	 * @param string $name {@from body} The field name
	 * @param array $request_data Used to disallow ID attribute. Should not be filled in the request.
	 *
	 * @return array {@link \Model\FieldType} attributes
	 */
	protected function put($id, $name = null, array $request_data = null) {
		return $this->_processPut($id, compact('name'), $request_data);
	}

}