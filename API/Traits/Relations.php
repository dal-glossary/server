<?php
namespace API\Traits;

use API\Responder;
use Luracast\Restler\RestException;

trait Relations {

	/**
	 * Utility method that encapsulates a list of database entries in a plain array, using {@link \ActiveRecord\Model::to_array}
	 * with {@link \API\Model::$customAttributes} as argument.
	 * Receives a list of model entries and returns a plain array, complaining if the given array is empty in form
	 * of a No Content HTTP response.
	 *
	 * @param \ActiveRecord\Model[] $entries list of models
	 * @param string $onlyField If should return only this specific field or all attributes from the model
	 * @throws \Luracast\Restler\RestException 204 No Content when the array is empty
	 * @return array
	 * Simple array of data from models. See $onlyField for details of what can be returned.
	 */
	protected static function _arrayFromModels(array $entries, $onlyField = '') {
		if (sizeof($entries) == 0)
			throw new RestException(HTTP_NO_CONTENT);

		$result = [];
		foreach ($entries as $entry)
			$result[] = $onlyField ? $entry->$onlyField : $entry->to_array(self::$customAttributes);

		return $result;
	}

	/**
	 * Useful for finding all records related to a specific model.
	 * This method returns a list of attributes for all models related to the specified model's record. The method
	 * works with all relation types, be it has-one, has-many or many-to-many, providing the given $model have the
	 * $relation correctly setup.
	 *
	 * Example: trying to get all definitions related to a specific page. There's the "Page" model, that have a relation
	 * called "definitions". The call would be this way: <code>self::_findMeBy('Page', $page_id, 'definitions').
	 *
	 * @param string $model class name of the desired model. The \Model namespace will be added internally.
	 * @param int $id ID to find the $model
	 * @param string $relation Relation that will be used to return the needed models. By default it's
	 * guessed by pluralizing the current class name.
	 * @throws RestException 204 In case the main $model has no related entries.
	 * @throws RestException 404 In case the main $model is not found.
	 * @return array
	 * In case of a has-one relation, list of attributes; in case of has-many or many-to-many, list of
	 * lists of attributes.
	 *
	 * @see \API\Model::_modelClass Depends on _modelClass to guess the relation name.
	 */
	protected static function _findMeBy($model, $id, $relation = null) {
		self::_prepareRelationData($model);

		if (!$relation) {
			$class    = self::_modelClass();
			$relation = strtolower(
							\ActiveRecord\Utils::human_attribute(
								\ActiveRecord\Utils::pluralize(
									substr($class,
										strrpos($class, '\\') + 1)
						)   )   );
		}

		$mainModel = $model::find($id, ['include' => [$relation]]);
		return self::_arrayFromModels($mainModel->$relation);
	}

	/**
	 * Returns a list of related models.
	 * General method of returning data from related models.
	 *
	 * @param string $relation name of the related class, without the namespace.
	 * @param mixed $id Identification of the related models. If it's a scalar value, will be used as value for an "id"
	 * field; if array, will be used directly in the query conditions.
	 * @param string $onlyField If should return only this specific field or all attributes from the model
	 * @throws RestException 204 In case there's no models to be returned.
	 * @return array
	 */
	protected function _getRelation($relation, $id, $onlyField = '') {
		self::_prepareRelationData($relation, $id);

		return self::_arrayFromModels($relation::all(['conditions' => $id]), $onlyField);
	}

	/**
	 * Creates a related entry in the database.
	 * @param string $relClass Relation class, without namespace
	 * @param array $data Data to be inserted as this relation
	 * @param string $msg Success message
	 * @return string $msg in case of success. The value of this method should be returned directly by the final API.
	 */
	protected function _addRelation($relClass, array $data, $msg) {
		self::_prepareRelationData($relClass);
		$relClass::create($data);
		return Responder::simpleMessage($msg);
	}

	/**
	 * Removes a related entry from the database.
	 * @param string $relClass Relation class, without namespace
	 * @param mixed $id Identification of the entry to be removed, be it it's (int) "id" field or a compound condition.
	 * @param string $msg Success message
	 * @return string
	 * Returns $msg in case of success. The value of this method should be returned directly by the final API. It will
	 * not verify if the entry(ies) already exist; in case of trying to delete a non-existent value, it will simply
	 * return success.
	 */
	protected function _deleteRelation($relClass, $id, $msg) {
		self::_prepareRelationData($relClass, $id);
		$relClass::delete_all(['conditions' => $id]);
		return Responder::simpleMessage($msg);
	}

	/**
	 * Simply adds the namespace to the $class and encapsulates the $id in an array, or returns it clean if it's
	 * already a clause array.
	 * @param string $class
	 * @param array|mixed $id
	 */
	protected static function _prepareRelationData(&$class = null, &$id = null) {
		$class = "\\Model\\$class";

		if (!is_array($id)) $id = ['id' => $id];
	}

}