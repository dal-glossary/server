<?php

use Phinx\Migration\AbstractMigration;

class BasicTables extends AbstractMigration {

	public function change() {

		$this->table('users')
			->addColumn('username', 'string', ['limit' => 50])
			->addColumn('first_name', 'string', ['limit' => 50])
			->addColumn('last_name', 'string', ['limit' => 100, 'null' => true])
			->addIndex('username', ['unique' => true])
			->create();

		$this->table('groups')
			->addColumn('name', 'string', ['limit' => 50])
			->addIndex('name', ['unique' => true])
			->create();

		$this->table('groups_have_users', ['id' => false, 'primary_key' => ['user_id', 'group_id']])
			->addColumn('user_id', 'integer')
			->addColumn('group_id', 'integer')
			->addForeignKey('user_id', 'users')
			->addForeignKey('group_id', 'groups')
			->create();

		$this->table('pages')
			->addColumn('name', 'string', ['limit' => 100])
			->addColumn('url', 'text')
			->addColumn('hashed_url', 'string', ['limit' => 32])
			->addIndex('hashed_url', ['unique' => true])
			->create();

		$this->table('groups_have_pages', ['id' => false, 'primary_key' => ['group_id', 'page_id']])
			->addColumn('group_id', 'integer')
			->addColumn('page_id', 'integer')
			->addForeignKey('group_id', 'groups')
			->addForeignKey('page_id', 'groups')
			->create();

		$this->table('words')
			->addColumn('name', 'string', ['limit' => 100])
			->addIndex('name', ['unique' => true])
			->create();

		$this->table('definitions')
			->addColumn('word_id', 'integer')
			->addForeignKey('word_id', 'words')
			->create();

		$this->table('pages_have_definitions', ['id' => false, 'primary_key' => ['page_id', 'definition_id']])
			->addColumn('page_id', 'integer')
			->addColumn('definition_id', 'integer')
			->addForeignKey('page_id', 'pages')
			->addForeignKey('definition_id', 'definitions')
			->create();

		$this->table('field_types')
			->addColumn('name', 'string', ['limit' => 50])
			->addIndex('name', ['unique' => true])
			->create();

		$this->table('definition_fields')
			->addColumn('definition_id', 'integer')
			->addColumn('field_type_id', 'integer')
			->addColumn('value', 'text')
			->addColumn('hashed_value', 'string', ['limit' => 32])
			->addColumn('creator_id', 'integer')
			->addColumn('votes', 'integer', ['default' => 0])
			->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
			->addColumn('updated_at', 'timestamp', ['null' => true])
			->addIndex(['definition_id', 'field_type_id', 'hashed_value'], ['unique' => true])
			->addForeignKey('definition_id', 'definitions')
			->addForeignKey('field_type_id', 'field_types')
			->addForeignKey('creator_id', 'users')
			->create();
		
	}
}