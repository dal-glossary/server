<?php

use Phinx\Migration\AbstractMigration;

class WordView extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->view('pages_have_words')
            ->setTableName("pages_have_definitions")
            ->addColumn("page_id", 'pages_have_definitions')
            ->addColumn("word_id", 'definitions')
            ->addInnerJoin("definitions", $this->condition()
                ->comparison("=")
                    ->column("definition_id", 'pages_have_definitions')
                    ->column("id", 'definitions')
            )->create();
    
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->view('pages_have_words')
            ->drop();
    }
}