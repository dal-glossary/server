<?php

/**
 * Logger class, that saves messages to various logging objects.
 * This static class is a Singleton wrapper to Monolog, providing easier logging functionalities through the entire
 * application. It's in the global namespace to make its use easier.
 *
 * @method log
 * @method msg
 * @method debug
 * @method info
 * @method warn
 * @method warning
 * @method error
 * @method critical
 * @method alert
 */
class Log {

	/**
	 * @var \Monolog\Logger
	 */
	static protected $instance;

	const DEBUG    = 100;
	const INFO     = 200;
	const WARN     = 300;
	const ERROR    = 400;
	const CRITICAL = 500;
	const ALERT    = 550;

	const LOG_DEBUG    = 'debug';
	const LOG_INFO     = 'info';
	const LOG_WARN     = 'warning';
	const LOG_ERROR    = 'error';
	const LOG_CRITICAL = 'critical';
	const LOG_ALERT    = 'alert';

	/**
	 * Useless, this method was added just to help PHP understand that the {@link log} method is not
	 * a constructor.
	 */
	protected function __construct() { }

	/**
	 * Starts the log objects.
	 * Receives one or more \Monolog\Handler object(s). For help on what handlers are available, visit
	 * https://github.com/Seldaek/monolog#handlers.
	 *
	 * @see https://github.com/Seldaek/monolog#handlers
	 *
	 * @param \Monolog\Handler|\Monolog\Handler[] $handlers
	 */
	static public function prepare($handlers = array()) {
		if (self::$instance) return;

		self::$instance = new \Monolog\Logger('server');

		if (!is_array($handlers)) $handlers = [$handlers];

		foreach ($handlers as $handler)
			self::$instance->pushHandler($handler);
	}

	/**
	 * Returns the logger instance for direct use.
	 *
	 * @return \Monolog\Logger
	 */
	static public function instance() {
		if (!self::$instance) self::prepare();

		return self::$instance;
	}

	/**
	 * Implements all the available logging methods.
	 *
	 * @param string $level one of the LOG_* constants + "warn"
	 * @param array $args An array containing the message and optional context
	 * @return bool
	 */
	static public function __callStatic($level, array $args) {
		if (in_array($level, ['log', 'msg']))
			$level = array_shift($args);

		if (is_numeric($level)) {
			return call_user_func_array([self::instance(), 'addRecord'], array_merge([$level], $args));
		}
		else {
			if (!in_array($level, [
					self::LOG_DEBUG, self::LOG_INFO, self::LOG_WARN, 'warn',
					self::LOG_ERROR, self::LOG_CRITICAL, self::LOG_ALERT
			])) {
				self::error("Tried to log a message with unknown level '$level'");
				$level = self::LOG_WARN;
			}
			else {
				if ($level == 'warn') $level = self::LOG_WARN;
			}

			if (is_array($args[0]))
				$args[0] = var_export($args[0], true);

			return call_user_func_array([self::instance(), 'add'.ucfirst($level)], $args);
		}
	}

}