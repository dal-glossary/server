-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glossary_test
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.2
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='-03:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `definition_fields`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `definition_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `definition_id` int(11) NOT NULL,
  `field_type_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `hashed_value` varchar(32) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `votes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `definitions_id` (`definition_id`,`field_type_id`,`hashed_value`),
  KEY `field_types_id` (`field_type_id`),
  KEY `creator_id` (`creator_id`),
  CONSTRAINT `definition_fields_ibfk_7` FOREIGN KEY (`field_type_id`) REFERENCES `field_types` (`id`),
  CONSTRAINT `definition_fields_ibfk_3` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`),
  CONSTRAINT `definition_fields_ibfk_6` FOREIGN KEY (`definition_id`) REFERENCES `definitions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `definition_fields`
--

INSERT INTO `definition_fields` VALUES (1,1,1,'Something wet',             'fad3eaa99440b9787cdcde9ce99464b6', 1, 10, '2013-06-18 01:02:03', null);
INSERT INTO `definition_fields` VALUES (2,1,1,'Makes you less thirsty',    '60c59396a20df5d4376d1b98f2a1f974', 2, 5, '2013-05-21 03:05:09', '2013-06-19 13:45:29 -3000');
INSERT INTO `definition_fields` VALUES (3,3,1,'Burns wildly',              '466c2b7f31bd65e40828d3dae44e5514', 1, 5, '2013-06-10 02:06:08', null);

--
-- Table structure for table `definitions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `definitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `words_id` (`word_id`),
  CONSTRAINT `definitions_ibfk_1` FOREIGN KEY (`word_id`) REFERENCES `words` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `definitions`
--

INSERT INTO `definitions` VALUES (1,1),(2,1),(3,2),(4,3);

--
-- Table structure for table `field_types`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_types`
--

INSERT INTO `field_types` VALUES (1,'Main definition');

--
-- Table structure for table `groups`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` VALUES (1,'Administrators'),(2,'Students');

--
-- Table structure for table `groups_have_pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_have_pages` (
  `group_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`page_id`),
  KEY `pages_id` (`page_id`),
  CONSTRAINT `groups_have_pages_ibfk_4` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  CONSTRAINT `groups_have_pages_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_have_pages`
--

INSERT INTO `groups_have_pages` VALUES (1,1),(1,2);

--
-- Table structure for table `groups_have_users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_have_users` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `groups_id` (`group_id`),
  CONSTRAINT `groups_have_users_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `groups_have_users_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_have_users`
--

INSERT INTO `groups_have_users` VALUES (1,1),(1,2),(2,1),(3,2);

--
-- Table structure for table `pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `hashed_url` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashed_url` (`hashed_url`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` VALUES (1,'Wikipedia - Brasil','http://pt.wikipedia.org/wiki/Brasil','01203b7a9a2a27c22bc828419d4b7dc9'),(2,'Dalhousie','http://www.dal.ca','ad920603a18be8a41207c0529200af45');

--
-- Table structure for table `pages_have_definitions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_have_definitions` (
  `page_id` int(11) NOT NULL,
  `definition_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`definition_id`),
  KEY `definitions_id` (`definition_id`),
  CONSTRAINT `pages_have_definitions_ibfk_4` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  CONSTRAINT `pages_have_definitions_ibfk_3` FOREIGN KEY (`definition_id`) REFERENCES `definitions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_have_definitions`
--

INSERT INTO `pages_have_definitions` VALUES (1,1),(1,3);

--
-- Table structure for table `phinxlog`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phinxlog` (
  `version` bigint(14) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` VALUES (20130518204210,'2013-05-20 17:46:24','2013-05-20 17:46:25'),(20130527003239,'2013-06-01 06:13:04','2013-06-01 06:13:06'),(20130612131705,'2013-06-12 16:32:30','2013-06-12 16:32:30');

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

INSERT INTO `users` VALUES (1,'ig655930@dal.ca','Igor','Santos'),(2,'yule@cs.dal.ca','Daniel','Yule'),(3,'johndoe@gmail.com','John','Rasmus Doe'),(4,'janedoe@gmail.com','Jane','Dana Doe');

--
-- Table structure for table `words`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `words`
--

INSERT INTO `words` VALUES (3,'Compound word'),(2,'Fire'),(1,'Water');
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-14 19:24:03
