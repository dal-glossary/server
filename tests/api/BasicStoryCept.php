<?php
$I = new ApiGuy($scenario);
$I->wantTo('get all available pages');
$I->amLoggedAs($existing_user['username'], $existing_user_password);
$I->sendGET('page');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseIsJson();
$I->seeResponseContainsJson([
	1 => [
		'id' => 2,
		'url' =>  "http://www.dal.ca",
		'name' => 'Dalhousie',
		'hashed_url' => 'ad920603a18be8a41207c0529200af45'
	]
]);

$I->amGoingTo('get all words for a page');
/* @todo implement testing */

$I->amGoingTo('ask the definitions for those words');
/* @todo implement testing */