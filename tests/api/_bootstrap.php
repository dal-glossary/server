<?php
require_once __DIR__.'/../../config/constants.php';

/* ****************************************************** USER ****************************************************** */
$existing_user_password = 'asdfasd';

$existing_user  = [
	'id'         => 1,
	'username'   => 'ig655930@dal.ca',
	'first_name' => 'Igor',
	'last_name'  => 'Santos',
];
$existing_user2 = [
	'id'         => 2,
	'username'   => 'yule@cs.dal.ca',
	'first_name' => 'Daniel',
	'last_name'  => 'Yule'
];
$new_user       = [
	'first_name' => 'John',
	'last_name'  => 'Ape Doe',
	'username'   => 'john_doe@dal.ca'
];
$users_in_group = [&$existing_user, &$existing_user2];

foreach (['existing_user', 'existing_user2', 'new_user'] as $var)
	${$var}['name'] = ${$var}['first_name'].' '.${$var}['last_name'];

/* ****************************************************** PAGE ****************************************************** */
$existing_page  = [
	'id'   => 1,
	'name' => 'Wikipedia - Brasil',
	'url'  => 'http://pt.wikipedia.org/wiki/Brasil',
];
$existing_page2 = [
	'id'   => 2,
	'name' => 'Dalhousie',
	'url'  => 'http://www.dal.ca',
];
$new_page       = [
	'url'  => 'http://www.google.ca',
	'name' => 'Google Canada',
];
$pages_in_group = [&$existing_page, &$existing_page2];

foreach (['existing_page', 'existing_page2', 'new_page'] as $var)
	${$var}['hashed_url'] = md5(${$var}['url']);

/* ****************************************************** WORD ****************************************************** */
$existing_word  = [
	'id'   => 1,
	'name' => 'Water'
];
$existing_word2 = [
	'id'   => 2,
	'name' => 'Fire'
];
$new_word       = [
	'name' => 'Tyrannosaurus Rex'
];


/* **************************************************** FIELD TYPE ************************************************** */
$existing_fieldType = [
	'id'   => 1,
	'name' => 'Main definition'
];
$new_fieldType      = [
	'name' => 'Synonyms'
];

/* **************************************************** DEFINITION ************************************************** */
$existing_definition  = [
	'id'      => 1,
	'word_id' => $existing_word['id'],
	'word'    => $existing_word
];
$existing_definition2 = [
	'id'      => 3,
	'word_id' => $existing_word2['id'],
	'word'    => $existing_word2
];
$new_definition       = [
	'word_id' => $existing_word['id']
];

$definitions_from_page = [$existing_definition, $existing_definition2];

$complete_definition  = [
	'id'      => 1,
	'word_id' => $existing_word['id'],
	'word'    => $existing_word,
	'fields'  => [
		[
			'id'            => 1,
			'definition_id' => $existing_definition['id'],
			'field_type_id' => $existing_fieldType['id'],
			'value'         => 'Something wet',
			'hashed_value'  => md5('Something wet'),
			'creator_id'    => $existing_user['id'],
			'votes'         => 10,
			'created_at'    => 'Tue, 18 Jun 2013 01:02:03 -0300',
			'updated_at'    => null,
			'field_type'    => $existing_fieldType,
			'creator'       => $existing_user
		],
		[
			'id'            => 2,
			'definition_id' => $existing_definition['id'],
			'field_type_id' => $existing_fieldType['id'],
			'value'         => 'Makes you less thirsty',
			'hashed_value'  => md5('Makes you less thirsty'),
			'creator_id'    => $existing_user2['id'],
			'votes'         => 5,
			'created_at'    => 'Tue, 21 May 2013 03:05:09 -0300',
			'updated_at'    => 'Wed, 19 Jun 2013 13:45:29 -0300',
			'field_type'    => $existing_fieldType,
			'creator'       => $existing_user2
		]
	]
];
$complete_definition2 = [
	'id'      => 3,
	'word_id' => $existing_word2['id'],
	'word'    => $existing_word2,
	'fields'  => [
		[
			'id'            => 3,
			'definition_id' => $existing_definition2['id'],
			'field_type_id' => $existing_fieldType['id'],
			'value'         => 'Burns wildly',
			'hashed_value'  => md5('Burns wildly'),
			'creator_id'    => $existing_user['id'],
			'votes'         => 5,
			'created_at'    => 'Mon, 10 Jun 2013 02:06:08 -0300',
			'updated_at'    => null,
			'field_type'    => $existing_fieldType,
			'creator'       => $existing_user
		]
	]
];

$complete_definitions_from_page = [$complete_definition, $complete_definition2];

/* ****************************************************** GROUP **************************************************** */
$existing_group  = [
	'id'   => 1,
	'name' => 'Administrators'
];
$existing_group2 = [
	'id'   => 2,
	'name' => 'Students'
];
$new_group       = [
	'name' => 'Professors'
];