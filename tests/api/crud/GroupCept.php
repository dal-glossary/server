<?php
use \Helper\Arrays;

$I = new ApiGuy($scenario);
$I->wantToTest('CRUD of groups');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('Create'); /* ***************************************************************************************** */
$I->sendPOST('group', $new_group);
$I->seeResponseCodeIs(HTTP_CREATED);
$I->seeResponseContainsJson($new_group);

$I->amGoingTo('Read/Find'); /* ************************************************************************************** */
$I->sendGET('group/'.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($existing_group);

$I->sendGET('group/-1');
$I->seeResponseCodeIs(HTTP_NOT_FOUND);
$I->seeResponseIsJson();

$I->sendGET('group/AAA');
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `id`');

$I->amGoingTo('Test pages accessible to group'); /* ************************************************************************ */
$I->sendGET('group/'.$existing_group['id'].'/page');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson([1,2]);

$I->sendDELETE(sprintf('group/%d/page/%d', $existing_group['id'], $existing_page['id']));
$I->seeResponseCodeIs(HTTP_OK);
$I->sendGET('group/'.$existing_group['id'].'/page');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson([2]);

$I->sendPOST(sprintf('group/%d/page/%d', $existing_group['id'], $existing_page['id']));
$I->seeResponseCodeIs(HTTP_OK);
$I->sendGET('group/'.$existing_group['id'].'/page');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson([1,2]);

$I->amGoingTo('Test users inside group'); /* ************************************************************************ */
$I->sendGET('group/'.$existing_group['id'].'/user');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson([1,2]);

$I->sendDELETE(sprintf('group/%d/user/%d', $existing_group['id'], $existing_user['id']));
$I->seeResponseCodeIs(HTTP_OK);
$I->sendGET('group/'.$existing_group['id'].'/user');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson([2]);

$I->sendPOST(sprintf('group/%d/user/%d', $existing_group['id'], $existing_user['id']));
$I->seeResponseCodeIs(HTTP_OK);
$I->sendGET('group/'.$existing_group['id'].'/user');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson([1,2]);

$I->amGoingTo('Update'); /* ***************************************************************************************** */
$updated_data = array_merge($existing_group, ['name' => 'Velociraptor']);

$I->sendPUT('group/'.$existing_group['id'], [ 'name' => $updated_data['name'] ]);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->sendGET('group/'.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->amGoingTo('Delete'); /* ***************************************************************************************** */
$I->sendDELETE('group/'.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_OK);

$I->sendGET('group/'.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->sendDELETE('group/'.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->amGoingTo('Test validation'); /* ******************************************************************************** */
$I->sendPOST('group', array_merge($new_group, ['id' => 10]));
$I->seeResponseCodeIs(HTTP_METHOD_NOT_ALLOWED);

$I->sendPOST('group', ['name' => '']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`name` is required but missing');

$I->sendPOST('group', $new_group);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeResponseContainsJson(['error' => ['details' => ['name' => ['must be unique']]]]);

$I->amGoingTo('Test authentication'); /* **************************************************************************** */
/* @todo implement AUTH TESTING */
$I->resetCookie(SESSION_NAME);
//$I->sendPOST('page', Arrays::blacklist((array)$page, 'hashed_url'));
//$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);