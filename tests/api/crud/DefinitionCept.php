<?php
use \Helper\Arrays;

$I = new ApiGuy($scenario);
$I->wantToTest('CRUD of root definitions');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('Create'); /* ***************************************************************************************** */
$I->sendPOST('definition', $new_definition);
$I->seeResponseCodeIs(HTTP_CREATED);
$I->seeResponseContainsJson($new_definition);

$I->amGoingTo('Read/Find'); /* ************************************************************************************** */
$I->sendGET('definition/'.$existing_definition['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($existing_definition);

//TODO: Failing because of https://github.com/Codeception/Codeception/issues/381
//$I->sendGET('definition/'.$existing_definition['id'].'?complete');
//$I->seeResponseCodeIs(HTTP_OK);
//$I->seeResponseContainsJson($complete_definition);

$I->sendGET('definition/?page='.$existing_page['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($definitions_from_page);

$I->sendGET('definition/-1');
$I->seeResponseCodeIs(HTTP_NOT_FOUND);
$I->seeResponseIsJson();

$I->sendGET('definition/AAA');
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `id`');

$I->amGoingTo('Update'); /* ************************************************************************************** */
$updated_data = array_merge($existing_definition, ['word_id' => $existing_word2['id'], 'word' => $existing_word2]);

$I->sendPUT('definition/'.$existing_definition['id'], [ 'word_id' => $updated_data['word_id'] ]);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson(Arrays::blacklist($updated_data, 'word'));

$I->sendGET('definition/'.$existing_definition['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->amGoingTo('Delete'); /* ************************************************************************************** */
$I->sendDELETE('definition/'.$existing_definition['id']);
$I->seeResponseCodeIs(HTTP_OK);

$I->sendGET('definition/'.$existing_definition['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->sendDELETE('definition/'.$existing_definition['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->amGoingTo('Test validation'); /* ******************************************************************************** */
$I->sendPOST('definition', array_merge($new_definition, ['id' => 10]));
$I->seeResponseCodeIs(HTTP_METHOD_NOT_ALLOWED);

$I->sendPOST('definition', ['word_id' => 0]);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`word_id` is required but missing');

$I->sendPOST('definition', ['word_id' => 257]);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `word_id`. Expecting valid ID');

$I->amGoingTo('Test authentication'); /* **************************************************************************** */
/* @todo implement AUTH TESTING */
$I->resetCookie(SESSION_NAME);
//$I->sendPOST('page', Arrays::blacklist((array)$page, 'hashed_url'));
//$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);