<?php
use \Helper\Arrays;

$I = new ApiGuy($scenario);
$I->wantToTest('CRUD of users');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('Create'); /* ***************************************************************************************** */
$I->sendPOST('user', Arrays::whitelist($new_user, ['username' , 'name']));
$I->seeResponseCodeIs(HTTP_CREATED);
$I->seeResponseContainsJson($new_user);

$I->amGoingTo('Read/Find'); /* ************************************************************************************** */
$I->sendGET('user/'.$existing_user['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($existing_user);

$I->sendGET('user/?group='.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($users_in_group);

$I->sendGET('user/-1');
$I->seeResponseCodeIs(HTTP_NOT_FOUND);
$I->seeResponseIsJson();

$I->sendGET('user/AAA');
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `id`');

$I->amGoingTo('Update'); /* ************************************************************************************** */
$updated_data = array_merge($existing_user, ['username' => 'updated_user']);

$I->sendPUT('user/'.$existing_user['id'], [ 'username' => $updated_data['username'] ]);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->sendGET('user/'.$existing_user['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->amGoingTo('Delete'); /* ************************************************************************************** */
$I->sendDELETE('user/'.$existing_user['id']);
$I->seeResponseCodeIs(HTTP_OK);

$I->sendGET('user/'.$existing_user['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->sendDELETE('user/'.$existing_user['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->amGoingTo('Test validation'); /* ******************************************************************************** */
$I->sendPOST('user', array_merge($new_user, ['id' => 10]));
$I->seeResponseCodeIs(HTTP_METHOD_NOT_ALLOWED);

$I->sendPOST('user', ['username' => 'john_doe']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`name` is required but missing');

$I->sendPOST('user', ['name' => 'John Doe']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`username` is required but missing');

$I->sendPOST('user', $new_user);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeResponseContainsJson(['error' => ['details' => ['username' => ['must be unique']]]]);

$I->amGoingTo('Test authentication'); /* **************************************************************************** */
/* @todo implement AUTH TESTING */
$I->resetCookie(SESSION_NAME);
//$I->sendPOST('user', Arrays::blacklist((array)$user, 'hashed_url'));
//$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);