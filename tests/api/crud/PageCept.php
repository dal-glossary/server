<?php
use \Helper\Arrays;

$I = new ApiGuy($scenario);
$I->wantToTest('CRUD of pages');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('Create'); /* ***************************************************************************************** */
$I->sendPOST('page', Arrays::blacklist($new_page, 'hashed_url'));
$I->seeResponseCodeIs(HTTP_CREATED);
$I->seeResponseContainsJson($new_page);

$I->amGoingTo('Read/Find'); /* ************************************************************************************** */
$I->sendGET('page/'.$existing_page['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($existing_page);

$I->sendGET('page/?group='.$existing_group['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($pages_in_group);

$I->sendGET('page/-1');
$I->seeResponseCodeIs(HTTP_NOT_FOUND);
$I->seeResponseIsJson();

$I->sendGET('page/AAA');
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `id`');

$I->amGoingTo('Test definitions of a page'); /* ********************************************************************* */
$I->sendGET('page/'.$existing_page['id'].'/definition');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($definitions_from_page);

//TODO: Failing because of https://github.com/Codeception/Codeception/issues/381
//$I->sendGET('page/'.$existing_page['id'].'/definition?complete');
//$I->seeResponseCodeIs(HTTP_OK);
//$I->seeResponseContainsJson($complete_definitions_from_page);

$removed_definition = array_shift($definitions_from_page);
$I->sendDELETE(sprintf('page/%d/definition/%d', $existing_page['id'], $existing_definition['id']));
$I->seeResponseCodeIs(HTTP_OK);
$I->sendGET('page/'.$existing_page['id'].'/definition');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($definitions_from_page);

array_unshift($definitions_from_page, $existing_definition);
$I->sendPOST(sprintf('page/%d/definition/%d', $existing_page['id'], $existing_definition['id']));
$I->seeResponseCodeIs(HTTP_OK);
$I->sendGET('page/'.$existing_page['id'].'/definition');
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($definitions_from_page);

$I->amGoingTo('Update'); /* ************************************************************************************** */
$updated_data = array_merge($existing_page, ['name' => 'Updated page']);

$I->sendPUT('page/'.$existing_page['id'], [ 'name' => $updated_data['name'] ]);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->sendGET('page/'.$existing_page['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->amGoingTo('Delete'); /* ************************************************************************************** */
$I->sendDELETE('page/'.$existing_page['id']);
$I->seeResponseCodeIs(HTTP_OK);

$I->sendGET('page/'.$existing_page['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->sendDELETE('page/'.$existing_page['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->amGoingTo('Test validation'); /* ******************************************************************************** */
$I->sendPOST('page', array_merge($new_page, ['id' => 10]));
$I->seeResponseCodeIs(HTTP_METHOD_NOT_ALLOWED);

$I->sendPOST('page', ['name' => 'Google', 'url' => 'www.google.com']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorDetails(['url' => ['Malformed URL']]);

$I->sendPOST('page', ['url' => 'www.google.com']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`name` is required but missing');

$I->sendPOST('page', ['name' => 'Google']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`url` is required but missing');

$I->sendPOST('page', $new_page);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeResponseContainsJson(['error' => ['details' => ['hashed_url' => ['must be unique']]]]);

$I->amGoingTo('Test authentication'); /* **************************************************************************** */
/* @todo implement AUTH TESTING */
$I->resetCookie(SESSION_NAME);
//$I->sendPOST('page', Arrays::blacklist((array)$page, 'hashed_url'));
//$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);