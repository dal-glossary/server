<?php
use \Helper\Arrays;

$I = new ApiGuy($scenario);
$I->wantToTest('CRUD of words');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('Create'); /* ***************************************************************************************** */
$I->sendPOST('word', $new_word);
$I->seeResponseCodeIs(HTTP_CREATED);
$I->seeResponseContainsJson($new_word);

$I->amGoingTo('Read/Find'); /* ************************************************************************************** */
$I->sendGET('word/'.$existing_word['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($existing_word);

$I->sendGET('word/-1');
$I->seeResponseCodeIs(HTTP_NOT_FOUND);
$I->seeResponseIsJson();

$I->sendGET('word/AAA');
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `id`');

$I->amGoingTo('Update'); /* ************************************************************************************** */
$updated_data = array_merge($existing_word, ['name' => 'Velociraptor']);

$I->sendPUT('word/'.$existing_word['id'], [ 'name' => $updated_data['name'] ]);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->sendGET('word/'.$existing_word['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->amGoingTo('Delete'); /* ************************************************************************************** */
$I->sendDELETE('word/'.$existing_word['id']);
$I->seeResponseCodeIs(HTTP_OK);

$I->sendGET('word/'.$existing_word['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->sendDELETE('word/'.$existing_word['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->amGoingTo('Test validation'); /* ******************************************************************************** */
$I->sendPOST('word', array_merge($new_word, ['id' => 10]));
$I->seeResponseCodeIs(HTTP_METHOD_NOT_ALLOWED);

$I->sendPOST('word', ['name' => '']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`name` is required but missing');

$I->sendPOST('word', $new_word);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeResponseContainsJson(['error' => ['details' => ['name' => ['must be unique']]]]);

$I->amGoingTo('Test authentication'); /* **************************************************************************** */
/* @todo implement AUTH TESTING */
$I->resetCookie(SESSION_NAME);
//$I->sendPOST('page', Arrays::blacklist((array)$page, 'hashed_url'));
//$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);