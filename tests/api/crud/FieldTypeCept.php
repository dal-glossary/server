<?php
use \Helper\Arrays;

$I = new ApiGuy($scenario);
$I->wantToTest('CRUD of field types');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('Create'); /* ***************************************************************************************** */
$I->sendPOST('fieldType', $new_fieldType);
$I->seeResponseCodeIs(HTTP_CREATED);
$I->seeResponseContainsJson($new_fieldType);

$I->amGoingTo('Read/Find'); /* ************************************************************************************** */
$I->sendGET('fieldType/'.$existing_fieldType['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($existing_fieldType);

$I->sendGET('fieldType/-1');
$I->seeResponseCodeIs(HTTP_NOT_FOUND);
$I->seeResponseIsJson();

$I->sendGET('fieldType/AAA');
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('invalid value specified for `id`');

$I->amGoingTo('Update'); /* ************************************************************************************** */
$updated_data = array_merge($existing_fieldType, ['name' => 'Related expressions']);

$I->sendPUT('fieldType/'.$existing_fieldType['id'], [ 'name' => $updated_data['name'] ]);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->sendGET('fieldType/'.$existing_fieldType['id']);
$I->seeResponseCodeIs(HTTP_OK);
$I->seeResponseContainsJson($updated_data);

$I->amGoingTo('Delete'); /* ************************************************************************************** */
$I->sendDELETE('fieldType/'.$existing_fieldType['id']);
$I->seeResponseCodeIs(HTTP_OK);

$I->sendGET('fieldType/'.$existing_fieldType['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->sendDELETE('fieldType/'.$existing_fieldType['id']);
$I->seeResponseCodeIs(HTTP_NOT_FOUND);

$I->amGoingTo('Test validation'); /* ******************************************************************************** */
$I->sendPOST('fieldType', array_merge($new_fieldType, ['id' => 10]));
$I->seeResponseCodeIs(HTTP_METHOD_NOT_ALLOWED);

$I->sendPOST('fieldType', ['name' => '']);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeErrorMessageContains('`name` is required but missing');

$I->sendPOST('fieldType', $new_fieldType);
$I->seeResponseCodeIs(HTTP_BAD_REQUEST);
$I->seeResponseContainsJson(['error' => ['details' => ['name' => ['must be unique']]]]);

$I->amGoingTo('Test authentication'); /* **************************************************************************** */
/* @todo implement AUTH TESTING */
$I->resetCookie(SESSION_NAME);
//$I->sendPOST('page', Arrays::blacklist((array)$page, 'hashed_url'));
//$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);