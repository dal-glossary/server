<?php
$I = new ApiGuy($scenario);
$I->wantTo('authenticate and fail authentication');

$I->amGoingTo('authenticate');
$I->amLoggedAs($existing_user['username'], $existing_user_password);

$I->amGoingTo('fail authentication');
$I->resetCookie(SESSION_NAME);
$I->sendPOST('user/login', ['user' => 'wrong', 'password' => 'very wrong']);
$I->seeResponseCodeIs(HTTP_UNAUTHORIZED);
$I->seeResponseIsJson();
$I->dontSeeValidToken();