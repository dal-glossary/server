<?php
namespace Codeception\Module;

/**
 * Custom methods for the {@link ApiGuy}
 * @package Codeception\Module
 */
class ApiHelper extends \Codeception\Module {

	public function sendPUT($url, $params = [], $files = []) {
		if (is_array($params))
			$params = json_encode($params);

		return $this->getModule('REST')->sendPUT($url, $params, $files);
	}

	public function amLoggedAs($user, $password) {
		$rest = $this->getModule('REST');
		$rest->sendPOST('user/login', ['user' => $user, 'password' => $password]);
		$rest->seeResponseCodeIs(HTTP_OK);
		$rest->seeResponseIsJson();
		$this->seeValidToken();
	}

	public function seeValidToken() {
		$this->getModule('PhpBrowser')->seeCookie(SESSION_NAME);
		$rest  = $this->getModule('REST');
		$token = $rest->grabDataFromJsonResponse('token');
		$this->assertEquals(SESSION_NAME, $token['name']);
		$this->assertNotEmpty($token['value']);
	}

	public function dontSeeValidToken() {
		$this->getModule('PhpBrowser')->dontSeeCookie(SESSION_NAME);
		$this->getModule('REST')->dontSeeResponseContainsJson(['token' => ['name' => SESSION_NAME]]);
	}

	protected function proceedErrorMessageIs($message) {
		return ['True', $this->getModule('REST')->seeResponseContainsJson(['error' => ['message' => $message]])];
	}

	public function seeErrorMessageIs($message) {
		$this->assert($this->proceedErrorMessageIs($message));
	}

	public function dontSeeErrorMessageIs($message) {
		$this->assertNot($this->proceedErrorMessageIs($message));
	}

	protected function proceedErrorMessageContains($message) {
		return ['Contains', $message, $this->getModule('REST')->grabDataFromJsonResponse('error.message')];
	}

	public function seeErrorMessageContains($message) {
		$this->assert($this->proceedErrorMessageContains($message));
	}

	public function dontSeeErrorMessageContains($message) {
		$this->assertNot($this->proceedErrorMessageContains($message));
	}

	public function seeErrorDetails(array $details) {
		$this->getModule('REST')->seeResponseContainsJson(['error' => ['details' => $details]]);
	}

	public function dontSeeErrorDetails(array $details) {
		$this->getModule('REST')->dontSeeResponseContainsJson(['error' => ['details' => $details]]);
	}

}