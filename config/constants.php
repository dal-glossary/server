<?php

/**
 * The environment variable GLOSSARY_ENV should be one of:
 * - development
 * - testing
 * - production
 * If not present, 'development' is considered the default environment.
 */
define('ENV', getenv('GLOSSARY_ENV') ? : 'development');

/**
 * Whether the system is in production mode or not.
 */
define('PROD', ENV == 'production');

/**
 * Shortcut for the system's directory separator
 */
define('DS', DIRECTORY_SEPARATOR);

/**
 * Application's root folder
 */
define('ROOT', dirname(dirname(__FILE__)).DS);

/**
 * WWW folder
 */
define('WWW', ROOT.'www');

/**
 * Whether HTTPS is enabled for this request
 */
define('HTTPS_ENABLED', !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off');

/**
 * The session ID used during logged operations. Should be used along with session_name()
 */
define('SESSION_NAME', 'GAS'); // Glossary Authenticated Session

define('MINUTE', 60);
define('HOUR', 60 * MINUTE);
define('DAY', 24 * HOUR);
define('MONTH', 30 * DAY);
define('YEAR', 365 * DAY);

// HTTP codes for success responses
const HTTP_OK         = 200;
const HTTP_CREATED    = 201;
const HTTP_NO_CONTENT = 204;

// HTTP codes for client errors
const HTTP_BAD_REQUEST        = 400;
const HTTP_UNAUTHORIZED       = 401;
const HTTP_FORBIDDEN          = 403;
const HTTP_NOT_FOUND          = 404;
const HTTP_METHOD_NOT_ALLOWED = 405;
const HTTP_TOO_MANY_REQUESTS  = 429;

// HTTP codes for server errors
const HTTP_INTERNAL_ERROR      = 500;
const HTTP_NOT_IMPLEMENTED     = 501;
const HTTP_SERVICE_UNAVAILABLE = 503;
