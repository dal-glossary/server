<?php
// Based on https://github.com/illuminate/database/blob/master/README.md

$phinx_config = \Symfony\Component\Yaml\Yaml::parse(ROOT.'phinx.yml');

$connections = [];
foreach ($phinx_config['environments'] as $name => $env) {
	if (!is_array($env)) continue;
	$connections[$name] = [
		'driver'    => $env['adapter'],
		'host'      => $env['host'],
		'port'      => $env['port'],
		'database'  => $env['name'],
		'username'  => $env['user'],
		'password'  => $env['pass'],
		'charset'   => 'utf8',
		'collation' => 'utf8_unicode_ci'
	];
}

\LaravelBook\Ardent\Ardent::configureAsExternal($connections[ENV]);