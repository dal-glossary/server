<?php
namespace Model;

/**
 * User model
 * @package Models
 *
 * @property integer id
 * @property username unique
 * @property first_name
 * @property last_name optional
 * @property name composed of "first_name last_name"
 *
 * @property GroupHasUser[] groups_have_users
 * @property Group[] groups
 * @property Page[] pages
 * @property DefinitionField[] definition_fields
 */
class User extends Base {

	static $has_many = [
		['groups_have_users', 'class_name' => 'GroupHasUser'],
		['groups',              'through' => 'groups_have_users'],
		['pages',               'through' => 'groups'],
		['definition_fields',   'foreign_key' => 'creator_id'],
	];

	static $validates_presence_of = ['username', 'first_name'];
	static $validates_uniqueness_of = [['username']];
	static $validates_length_of = [
		['username', 'maximum' => 50],
		['first_name', 'maximum' => 50],
		['last_name', 'maximum' => 100]
	];

	public function get_name() {
		return "$this->first_name $this->last_name";
	}

	public function set_name($name) {
		$this->first_name = strtok($name, ' ');
		$this->last_name = strtok('');
	}

	public function before_destroy() {
		foreach($this->groups_have_users as $rel)
			$rel->delete();

		/* TODO: Definition Fields SHOULD NOT be erased:
		 * 1. they should receive a mock value as user creator OR
		 * 2. we should use a "deleted" field to hide users instead of removing them
		 */
		foreach ($this->definition_fields as $def)
			$def->delete();
	}

}