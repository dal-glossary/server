<?php
namespace Model;

/**
 * DefinitionField model
 * @package Models
 *
 * @property integer id
 * @property integer definition_id
 * @property integer field_type_id
 * @property string value
 * @property string hashed_value
 * @property integer creator_id
 * @property integer votes
 * @property Date created_at
 * @property Date updated_at optional
 */
class DefinitionField extends Base {

	protected $hashed = 'value';
	use Traits\HashedField;

	static $belongs_to = [
		['field_type'],
		['definition'],
		['creator', 'class_name' => 'User', 'foreign_key' => 'creator_id'],
	];

	static $validates_presence_of = [
		'definition_id', 'field_type_id', 'value', 'hashed_value',
		'creator_id', 'votes', 'created_at'
	];
	static $validates_uniqueness_of = [
		[['field_type_id', 'definition_id', 'hashed_value'], 'message' => 'Definition already registered']
	];
	static $validates_length_of = [
		['value', 'minimum' => 1],
		['hashed_value', 'is' => 32],
	];
	static $validates_numericality_of = [
		['definition_id', 'only_integer' => true],
		['field_type_id', 'only_integer' => true],
		['creator_id', 'only_integer' => true],
		['votes', 'only_integer' => true],
	];
	static $validates_format_of = [
		['hashed_value', 'with' => MD5_REGEXP]
	];

	public function after_validation() {
		if ($this->errors->on('hashed_value') == 'must be unique') {
			$this->errors->add('value', 'must be unique');
		}
	}

}