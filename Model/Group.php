<?php
namespace Model;

/**
 * Group model
 * @package Models
 *
 * @property integer id
 * @property string name unique
 *
 * @property GroupHasUser[] groups_have_users
 * @property GroupHasPage[] groups_have_pages
 * @property User[] users
 * @property Page[] pages
 */
class Group extends Base {

	static $has_many = [
		['groups_have_users', 'class_name' => 'GroupHasUser'],
		['groups_have_pages', 'class_name' => 'GroupHasPage'],
		['users',              'through' => 'groups_have_users'],
		['pages',              'through' => 'groups_have_pages'],
	];

	static $validates_presence_of = ['name'];
	static $validates_uniqueness_of = [['name']];
	static $validates_length_of = [['name', 'maximum' => 50]];

	public function before_destroy() {
		foreach($this->groups_have_pages as $rel)
			$rel->delete();

		foreach($this->groups_have_users as $rel)
			$rel->delete();
	}
}