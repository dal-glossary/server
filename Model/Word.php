<?php
namespace Model;

/**
 * Word model
 * @package Models
 *
 * @property integer id
 * @property string name unique
 *
 * @property Definition[] $definitions
 */
class Word extends Base {

	public static $rules = [
		'name' => ['required', 'max:100']
	];

    public static $relationsData = [
        'definitions' => ['hasMany', '\Model\Definition']
    ];

    public static function beforeDelete($me) {
        $me->definitions()->detatch();
    }

}