<?php
namespace Model;

/**
 * Dumb M:N model
 * @package Models
 *
 * @property integer page_id
 * @property integer definition_id
 */
class PageHasWord extends Base {

	static $table_name = 'pages_have_words';

}