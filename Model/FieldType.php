<?php
namespace Model;

/**
 * FieldType model
 * @package Models
 *
 * @property integer id
 * @property string name unique
 */
class FieldType extends Base {

	static $has_many = [['definition_fields']];

	static $validates_presence_of = ['name'];
	static $validates_uniqueness_of = [['name']];
	static $validates_length_of = [['name', 'maximum' => 50]];

	/** TODO: Definition Fields SHOULD NOT be erased:
	 * 1. they should receive a mock value as field type OR
	 * 2. deletion of field types being used should be not allowed OR
	 * 3. we should use a "deleted" field to hide entries instead of removing them
	 */
	public function before_destroy() {
		foreach ($this->definition_fields as $def)
			$def->delete();
	}
}