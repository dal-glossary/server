<?php
namespace Model;
use Helper\Arrays;
use LaravelBook\Ardent\Ardent;

abstract class Base extends Ardent {

	protected static $unguarded = true;
	public $timestamps          = false;
	public $throwOnValidation   = true;
	public static $throwOnFind  = true;

	/**
	 * Get the table associated with the model, ignoring the Model namespace.
	 * @return string
	 */
	public function getTable() {
		if (isset($this->table)) return $this->table;
		return snake_case(str_plural(str_replace('Model\\', '', get_class($this))));
	}

	public function errors() {
		if (strpos(debug_backtrace()[1]['class'], 'InvalidModelException') !== false)
			return $this->validationErrors->toArray();
		else
			return parent::errors();
	}

	public function toArray() {
		$values = parent::toArray();
		return Arrays::blacklist($values, 'pivot');
	}

}