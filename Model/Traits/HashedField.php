<?php
namespace Model\Traits;

if(!defined('MD5_REGEXP')) define('MD5_REGEXP', '/^[\w\d]{32}$/');
if(!defined('MD5_RULE')) define('MD5_RULE', 'regex:'.MD5_REGEXP);

/**
 * Trait to be used along with models that require some field to have a copied, hashed version.
 * This trait requires the base class to have a property called "hashed" (usually protected). This
 * property should be a string (or an array of strings) with the name of the field(s) that requires
 * hashing. The model also needs another column with "hashed_" prefixed.
 *
 * The hash is currently implemented using simple MD5 encrypt method and the final model should still
 * include validation attributes if you want to make sure everything is fine. In PHP.ActiveRecord this
 * can be achieved using properties as follows in the model:
 * <code>
 *	static $validates_uniqueness_of = [ ['hashed_url'] ];
 *	static $validates_format_of = [ ['hashed_url', 'with' => '/^[\w\d]{32}$/'] ];
 * </code>
 *
 * Example: a model for Pages that have two fields requiring hashing, "url" and "title".
 * That class should have a property such as "protected $hashed = ['url', 'title'];"
 * and a total of four columns: url, hashed_url, title and hashed_title.
 *
 * @package Traits
 */
trait HashedField {

	/**
	 * @var bool
	 * Used to make sure we will not skip non-hashed fields when they're __set.
	 */
	private $hash_set = false;

	protected function hash($field, $value) {
		$this->attributes[$field] = $value;
		$this->attributes["hashed_$field"] = md5($value);
		$this->hash_set = true;
	}

	/**
	 * Determine if a set mutator exists for an attribute.
	 *
	 * @param  string  $key
	 * @return bool
	 */
	public function hasSetMutator($key) {
		if (method_exists($this, 'set'.studly_case($key).'Attribute')) {
			return true;
		}
		else {
			return ($this->isHashedField($key));
		}
	}

	public function __call($method, $args) {
		$this->hash_set = false;

		if (isset($this->hashed) && $this->hashed) {
			if (preg_match('/^set(.+)Attribute$/', $method, $attr)) {
				$attr = (static::$snakeAttributes)? snake_case($attr[1]) : $attr[1];
				if ($this->isHashedField($attr))
					$this->hash($attr, $args[0]);
			}

			//if hash_set is still false it means we haven't received a call for a hash field; go ahead.
			if (!$this->hash_set)
				return parent::__call($method, $args);
		}
		else {
			error_log('Using \Traits\HashedField without a "hashed" property is quite useless, eh?');
			return parent::__call($method, $args);
		}
	}

	/**
	 * Tells whether the field is marked as having a hashed brother or not.
	 * @param $field field name
	 * @return bool
	 */
	private function isHashedField($field) {
		return
			isset($this->hashed) && (
				(is_array($this->hashed) && in_array($field, $this->hashed)) ||
				(is_string($this->hashed) && $field == $this->hashed)
			);
	}
}