<?php
namespace Model;

use Helper\Arrays;
use Illuminate\Database\Eloquent\Collection;

/**
 * Definition model
 * @package Models
 *
 * @property integer              id
 * @property int                  word_id
 *
 * @property DefinitionField[]    $definition_fields
 * @property Page[]               $pages
 */
class Definition extends Base {

	public static $rules = [
		'word_id' => ['required', 'integer'],
	];

	public static $relationsData = [
		'fields' => ['hasMany', '\Model\DefinitionField'],
		'pages'  => ['belongsToMany', '\Model\Page', 'table' => 'pages_have_definitions'],
		'word'   => ['belongsTo', '\Model\Word']
	];

	public static function beforeDelete($me) {
		foreach ($me->fields as $field) $field->delete();
		foreach ($me->pages as $page) $page->pivot->delete();
	}

	public static function find_complete($cond, array $options = []) {
		$definitions = self::find($cond, array_merge_recursive($options, [
			'include' => [
				'word', 'fields' => ['field_type', 'creator']
			]
		]));

		if ($definitions instanceof self) $definitions = [$definitions];

		$result = [];
		foreach ($definitions as $definition) $result[] = $definition->to_array([
			'include' => [
				'word', 'fields' => [
					'include' => [
						'field_type', 'creator'
					]
				]
			]
		]);

		return (sizeof($result) == 1) ? $result[0] : $result;
	}

	public static function find_partial($ids_or_models) {
		if ($ids_or_models instanceof Collection)
			$definitions = $ids_or_models;
		else
			$definitions = self::with('word')->find($ids_or_models);

		if (!$definitions instanceof Collection)
			$definitions = [$definitions];

		$data = [];
		foreach ($definitions as $definition) $data[] = $definition->toArray();

		if (sizeof($data) == 1)
			$data = $data[0];

		return $data;
	}
}