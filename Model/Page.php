<?php
namespace Model;

/**
 * Page model
 * @package Models
 *
 * @property integer              id
 * @property string               name
 * @property string               url
 * @property string               hashed_url unique
 *
 * @property Definition[]         $definitions
 * @property User[]               $users
 * @property Group[]              $groups
 */
class Page extends \Illuminate\Database\Eloquent\Model {

public $table = 'pages';

	protected $hashed = 'url';
	use Traits\HashedField;

	static $has_many = [
		['groups_have_pages',       'class_name' => 'GroupHasPage'],
		['pages_have_definitions',  'class_name' => 'PageHasDefinition'],
		['pages_have_words',        'class_name' => 'PageHasWord'],
		['groups',      'through' => 'groups_have_pages'],
		['definitions', 'through' => 'pages_have_definitions'],
		['words',       'through' => 'pages_have_words'],
		['users',       'through' => 'groups'],
	];

	public static $customMessages = [
		'url.regex' => 'Malformed URL'
	];

	public static $relationsData = [
		'groups'      => ['belongsToMany', '\Model\Group',      'table' => 'groups_have_pages'],
		'definitions' => ['belongsToMany', '\Model\Definition', 'table' => 'pages_have_definitions'],
	];

	public static function beforeDelete($me) {
		$me->definitions()->detatch();
		$me->groups()->detatch();
	}

	/**
	 * @param Page $me
	 */
	public static function afterValidate($me) {
		if ($me->validationErrors->has('hashed_url')) {
			foreach($me->validationErrors->get('hashed_url') as $error) {
				if (strpos($error, 'has already been taken')) {
					$me->validationErrors->add('url', 'The URL has already been taken.');
					break;
				}
			}
		}
	}

	public function errors() {
		if (strpos(debug_backtrace()[1]['class'], 'InvalidModelException') !== false) {
			$errors = $this->validationErrors->toArray();
			if (array_key_exists('hashed_url', $errors)) unset($errors['hashed_url']);
			return $errors;
		}
		else {
			return parent::errors();
		}
	}

//	public function definitions() {
//		return $this->belongsToMany('\Model\Definition', 'pages_have_definitions');
//	}
}
