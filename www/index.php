<?php
/* ********************************************** LOADING CONFIG FILES ********************************************** */
$config_files = ['constants', '../vendor/autoload', 'database'];

foreach($config_files as $file)
	require_once '..'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.strtr("$file.php", '/', DIRECTORY_SEPARATOR);

/* ************************************************ LOADING API FILES *********************************************** */

Log::prepare([
	new \Monolog\Handler\RotatingFileHandler('..'.DS.'logs'.DS.'server.log', 15, Log::INFO)
]);

use \Luracast\Restler\Defaults;
use \Luracast\Restler\Resources;
use \Luracast\Restler\Restler;

$api_classes = ['User', 'Page', 'Word', 'Definition', 'FieldType', 'Group'];

$app = new \API\Restler;
$app->addAuthenticationClass('\API\User');
Defaults::$responderClass = '\API\Responder';

//Adding the explorer API, accessible at /api-doc
$app->addAPIClass('\Luracast\Restler\Resources');
Resources::$useFormatAsExtension = false;
Resources::$hideProtected = false;

foreach ($api_classes as $class)
	$app->addAPIClass("\\API\\$class");


//Allow Cross Origin Resource Sharing
Defaults::$crossOriginResourceSharing = true;

$app->handle(PROD);