<?php
const REMOTE_REPOSITORY = '/repo/glossary/server.git';
const DS                = DIRECTORY_SEPARATOR;

function run($command, $success, $failure) {
	$callable = is_string($command)?
		function() use ($command) { passthru($command, $result); return !$result; } : $command;

	if ($callable()) echo "==> $success\n";
	else            throw new Exception("==> $failure\n");
}

function abort_if_not_writable($name, $folder, $create = false) {
	if (!is_writable($folder)) {
		if ($create) {
			run(function () use ($folder) {
					return mkdir($folder, 0777);
				},
				"Created folder $folder",
				"Unable to create $name folder: $folder");
		}
		else throw new Exception("==> Unable to write to $name folder: $folder");
	}
	else {
		if ($create) echo "==> Folder $folder exists and is writable\n";
	}
}


desc('Runs all "install:" commands');
task('install', 'install:hooks', 'install:apache', 'install:folders', 'install:deps', 'install:db');

group('install', function () {

	desc('Updates the Git hooks');
	task('hooks', function () {
		$hooks_dir = REMOTE_REPOSITORY.DS.'hooks'.DS;
		abort_if_not_writable('Git hooks', $hooks_dir);

		foreach (scandir('config') as $file) {
			if (strpos($file, 'hook-') === 0)
				run(function () use ($file, $hooks_dir) {
					return copy('config'.DS.$file, $hooks_dir.substr($file, 5));
				}, "Hook $file updated", "Unable to update hook $file");
		}
	});

	desc('Prepare the Apache virtual host');
	task('apache', function () {
		$vhost_dir  = '/etc/apache2/sites-available';
		$vhost_file = 'glossary-server.vhost';
		abort_if_not_writable('Apache VirtualHosts', $vhost_dir);

		run(function () use ($vhost_dir, $vhost_file) {
			//@todo should replace the server path here with a decent one
			return copy('config'.DS.$vhost_file, $vhost_dir.DS.strtok($vhost_file, '.'));
		}, "VHost updated", "Unable to copy VHost file");


		run(function () {
			foreach (['a2ensite glossary-server', 'service apache2 reload'] as $command) {
				exec($command, $output, $result);
				if (!$result && strpos($output[0], 'already enabled') === false) {
					echo implode("\n", $output)."\n";
					return false;
				}
			}
			return true;
		}, "VHost reloaded", "Unable to reload Apache settings");
	});

	desc('Prepare any needed folder');
	task('folders', function () {
		abort_if_not_writable("Logs", 'logs', true);
	});

	desc('Updates the dependencies through Composer');
	task('deps', function () {
		run('composer update', 'Dependencies updated', 'Unable to run Composer to update dependencies');
	});

	desc('Runs database migrations');
	task('db', function() {
		run('vendor/bin/phinx migrate -e testing', 'Database migrations executed', 'Unable to run database migrations');
	});

});

//@todo to be implemented
group('test', function() {

});